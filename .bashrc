#bash config

# empty indicates unlimited history
HISTSIZE=
HISTFILESIZE=
#path
export PATH="/home/marius/.local/bin:/home/marius/.local:$PATH"
export PATH="/home/marius/utils/workflow:$PATH"
export PATH="$(find ~/scripts/utils/ -type d -not -path "*.git*" 2&> /dev/null | tr '\n' ':')$PATH"

#export JAVA_HOME="/usr/lib/jvm/default"
export IDEA_JDK="/usr/lib/jvm/jdk-jetbrains"
export CLION_JDK="$IDEA_JDK"

export _JAVA_OPTIONS="-Xmx6g"

#editor
export EDITOR="vim"
export VISUAL="vim"

export TERM="xterm-256color"

export BROWSER="firefox"

# envvars
export DOCKER_VOLUMES="$HOME/bulk/docker/volumes"
export SCHOOL_DIR="$HOME/documents/school/5"

# wine
export WINEPREFIX="$HOME/bulk/wine"

#flatpak shortcuts
alias steam='flatpak run com.valvesoftware.Steam &'

#shortcuts
source ~/.alias

function cjd() {
  pushd ~/*/*/${1}*
}

function ljs() {
  ls ${1} | grep -P --color=none '\d0-\d9.*$'
}

#terminal effects
Reset=$'\[\e[0m\]'
Bold=$'\[\e[1m\]'
Black=$'\[\e[30m\]'
Red=$'\[\e[31m\]'
Green=$'\[\e[32m\]'
Yellow=$'\[\e[33m\]'
Blue=$'\[\e[34m\]'
Magenta=$'\[\e[35m\]'
Cyan=$'\[\e[36m\]'
White=$'\[\e[97m\]'
Fore=$'\[\e[39m\]'

function in_git_repo {
    BRANCH=`git branch 2> /dev/null`
    if [ ! "${BRANCH}" == "" ]
    then
        echo "()"
    else
        echo ""
    fi
}

function dir_indicator {
    echo "$(dirs +0)" | rev | cut -d'/' -f2,1 | rev
}

export PS1=" ${Blue}\$(dir_indicator)${Fore} ${Red}\$(in_git_repo)${Fore} ${Green}»${Fore} "
