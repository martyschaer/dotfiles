#!/bin/sh
w="#FFFFFFFF"
t="#00000000"
r="#ff5995ff" 
b="#0c73c2ff"
g="#82b414ff"

i3lock -B 5 \
   --date-str="" \
   --time-pos='x+w/2:y+h/2' \
   --clock \
   --time-str="%H:%M" \
   --time-color="$w" \
   --time-font="Open Sans Light" \
   --time-size=72 \
   --line-color="$t" \
   --ring-color="$w" \
   --inside-color="$t" \
   --ringver-color="$b" \
   --insidever-color="$t" \
   --ringwrong-color="$r" \
   --insidewrong-color="$t" \
   --separator-color="$g" \
   --keyhl-color="$g" \
   --bshl-color="$r" \
   --wrong-text="" \
   --verif-text="" \
   --noinput-text="" \
   --radius=200 \
   --ring-width=3 \
   --ind-pos="x+w/2:y+h/2-25" \
