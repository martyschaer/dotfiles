#!/bin/bash

# This script chooses one of the files in this directory at random,
# and copies it to "wallpaper".
# This file is then used by i3

wallpaper="$(bash $HOME/scripts/utils/workflow/random.sh ".*\.png$" ~/.wallpapers)"
cp "$wallpaper" "$HOME/.wallpapers/wallpaper"
