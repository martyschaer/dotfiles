set backupdir=.backup/,~/.vimtmp/.backup/,/tmp//
set directory=.swp/,~/.vimtmp/.swp/,/tmp//
set undodir=.undo/,~/.vimtmp/.undo/,/tmp//
set nocompatible
set number
imap jk <Esc>
syntax on
filetype plugin indent on
set tabstop=2
set expandtab
colorscheme elflord
set colorcolumn=80
set laststatus=2
set statusline=%f\ %h%w%m%r\ %=%(%l,%c%V\ %=\ %P%)\ %{wordcount().words}\ words

                          inoremap <F10>  <Esc>/<++><Enter>"_c4l
                          inoremap <F11>  <Esc>?<++><Enter>"_c4l

                          inoremap ;a     ä
                          inoremap ;o     ö
                          inoremap ;u     ü
                          inoremap ;A     Ä
                          inoremap ;O     Ö
                          inoremap ;U     Ü

autocmd FileType markdown inoremap ;l     \label{}<Space><++><Esc>F}i
autocmd FileType markdown inoremap ;rn    \ref{}<Space><++><Esc>F}i
autocmd FileType markdown inoremap ;re    \eqref{}<Space><++><Esc>F}i
autocmd FileType markdown inoremap ;meq   \begin{equation}\label{}<cr><++><cr>\end{equation}<Esc>?}<cr>i

autocmd FileType html     inoremap ;l     <a rel="noopener noreferrer" target="_blank" href="+"><++></a><++><Esc>F+;;;;s

autocmd FileType tex      inoremap ;fr    \begin{frame}<cr>\frametitle{<+++>}<cr>\framesubtitle{}<cr><++><cr>\end{frame}<Esc>?<+++><Enter>"_c5l
autocmd FileType tex      inoremap ;item  \begin{itemize}<cr>\item <++><cr>\end{itemize}<Esc>?<++><Enter>"_c4l
autocmd FileType tex      inoremap ;enum  \begin{enumerate}<cr>\item <++><cr>\end{enumerate}<Esc>?<++><Enter>"_c4l
