# DotFiles

The configuration files for my computers, as well as a script that sym-links them all to the correct locations.

This is all fairly specific to my setup, so your mileage may vary.
