#!/bin/bash

#kill all running polybars
killall -q polybar

#wait for them to die
while pgrep -x polybar > /dev/null; do sleep 1; done

#launch new bars
polybar bar -c "$HOME/.config/polybar/config" &

echo "Bars launched..."
