#!/bin/bash
SCRIPT=$(readlink -f "$0" | grep -Po "\/(\w+\/)+")
SCRIPT=${SCRIPT%?}
HM="/home/m"

# symlink basics
ln -sf "$SCRIPT/.vimrc" "$HM/.vimrc"
ln -sf "$SCRIPT/.vimrc" "$HM/.ideavimrc"
ln -sf "$SCRIPT/.bashrc" "$HM/.bashrc"
ln -sf "$SCRIPT/.gitconfig" "$HM/.gitconfig"
ln -sf "$SCRIPT/.xinitrc" "$HM/.xinitrc"
ln -sf "$SCRIPT/.sqliterc" "$HM/.sqliterc"
ln -sf "$SCRIPT/.alias" "$HM/.alias"

# create directories for configuration
CONFIG="$HM/.config"

mkdir -p "$CONFIG/neofetch"
mkdir -p "$CONFIG/polybar"
mkdir -p "$CONFIG/htop"
mkdir -p "$CONFIG/i3"
mkdir -p "$CONFIG/ranger"

# symlink configs into the correct directories
ln -sf "$SCRIPT/neofetch/config" "$CONFIG/neofetch/config"
ln -sf "$SCRIPT/polybar/config" "$CONFIG/polybar/config"
ln -sf "$SCRIPT/polybar/launch.sh" "$CONFIG/polybar/launch.sh"
ln -sf "$SCRIPT/htop/config" "$CONFIG/htop/config"
ln -sf "$SCRIPT/i3/config" "$CONFIG/i3/config"
ln -sf "$SCRIPT/ranger/rc.conf" "$CONFIG/ranger/rc.conf"
ln -sf "$SCRIPT/ranger/rifle.conf" "$CONFIG/ranger/rifle.conf"

# symlink keyboard config
# This is commented out because I use the standard US layout now
#sudo ln -sf "$SCRIPT/00-keyboard.conf" "/etc/X11/xorg.conf.d/00-keyboard.conf"

# symlink wallpapers
ln -sf "$SCRIPT/.wallpapers" "$HM/.wallpapers"
